package org.bindiego.solr.pusher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import org.bindiego.solr.server.BindiegoSolrServer;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

/**
 * Created by wubin on 14-9-28.
 */
public class DataPusher {
    static final Logger logger = LogManager.getFormatterLogger(DataPusher.class.getName());

    private SolrServer server;

    public DataPusher(final String url) {
        this.server = BindiegoSolrServer.getSolrServer(url);
    }

    /**
     * Index a single Solr Document
     * @param document
     */
    public void addDoc(SolrInputDocument document) {
        try {
            server.add(document);

            doCommit();
        } catch (SolrServerException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        }
    }

    public void addDocs(Collection<SolrInputDocument> docs) {
        try {
            server.add(docs);

            doCommit();
        } catch (SolrServerException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        }
    }

    public <T> void addBean(T bean) {
        try {
            server.addBean(bean);

            doCommit();
        } catch (IOException e) {
            logger.error("", e);
        } catch (SolrServerException e) {
            logger.error("", e);
        }
    }

    public <T> void addBeans(List<T> beans) {
        try {
            server.addBeans(beans);

            doCommit();
        } catch (IOException e) {
            logger.error("", e);
        } catch (SolrServerException e) {
            logger.error("", e);
        }
    }

    /**
     * Delete by single id
     * @param idName
     * @param id
     */
    public void deleteById(String idName, Object id) {
        try {
            server.deleteByQuery(idName + ":" + id.toString());

            doCommit();
        } catch (SolrServerException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        }
    }

    /**
     * Bulk deletion by ids
     * @param idName
     * @param ids
     * @param <T>
     */
    public <T> void deleteByIds(String idName, List<T> ids) {
        StringBuffer query = null;

        if (ids.size() > 0) {
            query = new StringBuffer(idName + ":" + ids.get(0).toString());

            for (int i = 1; i < ids.size(); ++i) {
                if (ids.get(i) != null) {
                    query.append(" OR " + idName + ":" + ids.get(i).toString());
                }
            }
        }

        try {
            server.deleteByQuery(query.toString());

            doCommit();
        } catch (SolrServerException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        }
    }

    /**
     * Delete based on the string query searching results
     * @param query
     */
    public void deleteByQuery(final String query) {
        try {
            server.deleteByQuery(query);

            doCommit();
        } catch (SolrServerException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        }
    }

    /**
     * clear all index
     */
    public void deleteAllIndex() {
        try {
            logger.warn("All index will be wiped out.");

            server.deleteByQuery("*:*");

            doCommit();

            logger.warn("All index have been deleted.");
        } catch (IOException e) {
            logger.error("", e);
        } catch (SolrServerException e) {
            logger.error("", e);
        }
    }

    public void doCommit() {
        try {
            server.commit();
            //server.commit(false, false);
        } catch (SolrServerException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        }
    }
}
