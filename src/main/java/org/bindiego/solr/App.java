package org.bindiego.solr;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.CommonParams;
import org.bindiego.solr.data.Journal;
import org.bindiego.solr.data.JournalInput;
import org.bindiego.solr.data.paging.Pagination;
import org.bindiego.solr.puller.DataPuller;
import org.bindiego.solr.pusher.DataPusher;

import java.util.ArrayList;
import java.util.List;

public class App
{
    static final Logger logger = LogManager.getFormatterLogger(App.class.getName());

    static final String host = "localhost";
    //static final String host = "192.168.56.102";

    public static void main(String... args) {
        logger.info("Gamma ray burst started");

        getAndPush();
        /* change default path
        SolrQuery query = new SolrQuery();
        query.setParam(CommonParams.QT, "/terms");
        */

        logger.info("Gamma ray burst ended");
    }

    private static void tests() {
        double a = 23.2;
        double b = 23.8;

        logger.debug("a = " + a);
        logger.debug("b = " + b);
        logger.debug("Math.round(a) = " + Math.round(a));
        logger.debug("Math.round(b) = " + Math.round(b));
        logger.debug("Math.ceil(a) = " + Math.ceil(a));
        logger.debug("Math.ceil(b) = " + Math.ceil(b));
        logger.debug("Math.floor(a) = " + Math.floor(a));
        logger.debug("Math.floor(b) = " + Math.floor(b));
    }

    private static void getAndPush() {
        DataPuller puller = new DataPuller("http://" + host + ":8080/solr/journal_general");
        DataPusher pusher = new DataPusher("http://"+ host + ":8080/solr/bw_journal");

        // clean up
        pusher.deleteAllIndex();
        logger.debug("bw_journal cleared.");

        long num = puller.getTotalCount();
        logger.debug("Journals found in journal_general: " + num);

        int pageSize = 1000;
        int totalPages = new Double(Math.ceil(num / pageSize)).intValue();

        SolrQuery query = new SolrQuery(("*:*"));
        for (int pageNo = 1; pageNo <= totalPages; ++pageNo) {
            Pagination<JournalInput> pagination = puller.getBeans(query, pageNo, pageSize, null, JournalInput.class);

            List<Journal> journals = new ArrayList<Journal>();

            for (JournalInput ji : pagination.getList()) {
                Journal j = new Journal();

                j.setTitle(ji.getTitle());
                j.setIssn(ji.getIssn());
                //j.setAbbr(null);

                journals.add(j);
            }

            pusher.addBeans(journals);

            /*
            List<SolrInputDocument> journals = new ArrayList<SolrInputDocument>();

            for (JournalInput ji : pagination.getList()) {
                SolrInputDocument j = new SolrInputDocument();

                j.addField("title", ji.getTitle());
                j.addField("issn", ji.getIssn());
                //j.setAbbr(null);

                journals.add(j);
            }

            pusher.addDocs(journals);
            */
        }
    }
}
