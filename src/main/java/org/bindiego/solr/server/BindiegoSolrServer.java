package org.bindiego.solr.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.impl.XMLResponseParser;

/**
 * Created by wubin on 14-9-28.
 */
public class BindiegoSolrServer implements Cloneable {
    static final Logger logger = LogManager.getFormatterLogger(BindiegoSolrServer.class.getName());

    //private static HttpSolrServer server = null;
    private static SolrServer server = null;

    private BindiegoSolrServer() {}

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    public static SolrServer getSolrServer(final String url) {
        //if (server == null) {
            //server = new HttpSolrServer(url);
            //server = new CloudSolrServer(server);

            /* HttpSolrServer settings
            server.setMaxRetries(1); // defaults to 0.  > 1 not recommended.

            server.setConnectionTimeout(5000); // 5 seconds to establish TCP

            // Setting the XML response parser is only required for cross
            // version compatibility and only when one side is 1.4.1 or
            // earlier and the other side is 3.1 or later.
            //server.setParser(new XMLResponseParser()); // binary parser is used by default

            // The following settings are provided here for completeness.
            // They will not normally be required, and should only be used
            // after consulting javadocs to know whether they are truly required.
            server.setSoTimeout(1000);  // socket read timeout

            server.setDefaultMaxConnectionsPerHost(100);

            server.setMaxTotalConnections(100);

            server.setFollowRedirects(false);  // defaults to false

            // allowCompression defaults to false.
            // Server side must support gzip or deflate for this to have any effect.
            server.setAllowCompression(true);
            */
        //}

        //return server;
        return new HttpSolrServer(url);
    }
}
