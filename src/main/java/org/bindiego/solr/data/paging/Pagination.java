package org.bindiego.solr.data.paging;

import java.util.List;

/**
 * Created by wubin on 14-9-28.
 */
public class Pagination<T> extends SimplePage implements java.io.Serializable,
        Paginable {
    public Pagination() {
    }

    public Pagination(int pageNo, int pageSize, int totalCount) {
        super(pageNo, pageSize, totalCount);
    }

    public Pagination(int pageNo, int pageSize, int totalCount, List<T> list) {
        super(pageNo, pageSize, totalCount);
        this.list = list;
    }

    public int getFirstResult() {
        return this.totalCount == 0 ? 0 : (pageNo - 1) * pageSize + 1;
    }

    public int getLastResult() {
        int tempLastResult;
        tempLastResult = this.getFirstResult() + this.pageSize - 1;
        return tempLastResult > this.getTotalCount() ? this.getTotalCount()
                : tempLastResult;
    }

    public static int calculatePageNo(int firstResult, int pageSize) {
        return firstResult / pageSize + 1;
    }

    /**
     * 当前页的数据
     */
    private List<T> list;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
