package org.bindiego.solr.data.paging;

import org.apache.solr.client.solrj.SolrQuery.ORDER;

/**
 * Created by wubin on 14-9-28.
 */
public class OrderBy {
    private String[] fields = new String[0];
    private ORDER order;

    public String[] getFields() {
        return fields;
    }

    public ORDER getOrder() {
        return order;
    }

    protected OrderBy(ORDER ORDER, String... fields) {
        this.order = ORDER;
        this.fields = fields != null ? fields : this.fields;
    }

    public static OrderBy asc(String... fields) {
        return new OrderBy(ORDER.asc, fields);
    }

    public static OrderBy desc(String... fields) {
        return new OrderBy(ORDER.desc, fields);
    }

        /*
         * public String toString() { StringBuffer sb = new StringBuffer(); for
         * (String filed : this.fields) { sb.append(filed).append(','); }
         * sb.deleteCharAt(sb.length() - 1).append(" ").append( this.ORDER ==
         * ORDER.ASC ? "ASC" : "DESC"); return sb.toString(); }
         */

    public String getSidx() {
        StringBuffer sb = new StringBuffer();
        for (String filed : this.fields) {
            sb.append(filed).append(',');
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    @SuppressWarnings("static-access")
    public String getSord() {
        return this.order == order.asc ? "ASC" : "DESC";
    }

    public boolean validate() {
        if (this.fields == null)
            return false;
        if (this.fields.length == 0)
            return false;
        if (this.order == null)
            return false;
        return true;
    }
}
