package org.bindiego.solr.data;

import org.apache.solr.client.solrj.beans.Field;

/*
{
  "responseHeader": {
    "status": 0,
    "QTime": 0,
    "params": {
      "q": "*:*",
      "indent": "true",
      "wt": "json",
      "_": "1411892157829"
    }
  },
  "response": {
    "numFound": 28544,
    "start": 0,
    "docs": [
      {
        "issn": "1340-5705",
        "frequency": 91,
        "impact_factor": 0.156,
        "publisher": "Graduate School of Mathematical Sciences, The University of Tokyo",
        "source": "Graduate School of Mathematical Sciences, The University of Tokyo",
        "id": 1,
        "cover": "",
        "title": "JOURNAL OF MATHEMATICAL SCIENCES-THE UNIVERSITY OF TOKYO",
        "title_sort": "JOURNAL OF MATHEMATICAL SCIENCES-THE UNIVERSITY OF TOKYO",
        "eissn": "",
        "description": "The policy of the Journal of Mathematical Sciences, the University of Tokyo is to publish original research papers in the mathematical sciences including pure and applied mathematics. Further, it is also our policy to publish the journal in a paper format as well as electronically on the Internet. Precisely speaking, manuscripts older than one year are made available on our home page in a PDF format.",
        "aims": "",
        "forcode": "",
        "generic_image": "1",
        "url": "http://journal.ms.u-tokyo.ac.jp/index.html",
        "pissn": "1340-5705",
        "address": "GRADUATE SCH MATHEMATICAL SCIENCES UNIV TOKYO, 3-8-1 KOMABA, MEGURO-KU, TOKYO, JAPAN, 153-8914",
        "scie": false,
        "forname": "",
        "_version_": 1480011178746839000
      },
      {
        "issn": "0960-3174",
        "frequency": 1,
        "impact_factor": 1.977,
        "publisher": "Springer",
        "source": "Springer",
        "id": 2,
        "cover": "http://images.springer.com/sgw/journals/medium/11222.jpg",
        "title": "Statistics and Computing",
        "title_sort": "Statistics and Computing",
        "eissn": "1573-1375",
        "description": "Statistics and Computing is a bi-monthly refereed journal that publishes papers covering the interface between the statistical and computing sciences.<p/>The journal includes techniques for evaluating analytically intractable problems, such as bootstrap resampling, Markov chain Monte Carlo, sequential Monte Carlo, approximate Bayesian computation,Â  search and optimization methods, stochastic simulation and Monte Carlo, graphics, computer environments, statistical approaches to software errors, information retrieval, machine learning, statistics of databases and database technology, huge data sets and big data analytics, computer algebra, graphical models, image processing, tomography, inverse problems and uncertainty quantification.",
        "access_model": "hybrid",
        "aims": "Statistics and Computing is a bi-monthly refereed journal which publishes papers covering the range of the interface between theÂ statistical and computing sciences.<br/><br/>In particular, it addresses the use of statistical concepts in computing science, for example in machine learning, computer vision and data analytics, as well as the use of computers in data modelling, prediction and analysis. SpecificÂ topics which are covered include: techniques for evaluating analytically intractable problems such as bootstrap resampling, Markov chain Monte Carlo, sequential Monte Carlo, approximate Bayesian computation, Â search and optimization methods, stochastic simulation and Monte Carlo,Â graphics, computer environments, statistical approaches to software errors, information retrieval,Â machine learning,Â statistics of databases and database technology, huge data sets and big data analytics, computer algebra, graphical models, imageÂ processing, tomography, inverse problems and uncertainty quantification.<br/><b",
        "forcode": "0104",
        "generic_image": "0",
        "url": "http://www.springer.com/journal/11222",
        "pissn": "0960-3174",
        "address": "",
        "scie": true,
        "forname": "Statistics ",
        "_version_": 1480011178790879200
      },
      {
        "issn": "0969-0239",
        "frequency": 1,
        "impact_factor": 3.476,
        "publisher": "Springer",
        "source": "Springer",
        "id": 3,
        "cover": "http://images.springer.com/sgw/journals/medium/10570.jpg",
        "title": "Cellulose",
        "title_sort": "Cellulose",
        "eissn": "1572-882X",
        "description": "Cellulose is an international journal devoted to the dissemination of research and scientific and technological progress in the field of cellulose and related naturally occurring polymers. The journal is concerned with the pure and applied science of cellulose and related materials, and also with the development of relevant new technologies. This includes the chemistry, biochemistry, physics and materials science of cellulose and its sources, including wood and other biomass resources, and their derivatives. Coverage extends to the conversion of these polymers and resources into manufactured goods, such as pulp, paper, textiles, and manufactured as well natural fibers, and to the chemistry of materials used in their processing. Cellulose publishes review articles, research papers, and technical notes.",
        "access_model": "hybrid",
        "aims": "Cellulose is a bimonthly international journal devoted to the dissemination of research and scientific and technological progress in the field of cellulose and related naturally occurring polymers. This includes the chemistry, biochemistry, physics and materials science of cellulose and its sources, including wood and other biomass resources, and their derivatives. It also includes aspects of the conversion of these polymers and resources into manufactured goods, such as pulp, paper, textiles, and manufactured as well natural fibers, and to the chemistry of materials used in their processing. Cellulose publishes review articles, research papers, and technical notes.The Journal is concerned with the pure and applied science of cellulose and related materials, and also with the development of relevant new technologies. Appropriate areas are materials applications, applications for biological and biotechnological purposes, and nutritional, energy and fuels applications. ",
        "forcode": "0303",
        "generic_image": "0",
        "url": "http://www.springer.com/journal/10570",
        "pissn": "0969-0239",
        "address": "",
        "scie": true,
        "forname": "Macromolecular And Materials Chemistry",
        "_version_": 1480011178803462100
      },
      {
        "issn": "0920-1742",
        "frequency": 1,
        "impact_factor": 1.545,
        "publisher": "Springer",
        "source": "Springer",
        "id": 4,
        "cover": "http://images.springer.com/sgw/journals/medium/10695.jpg",
        "title": "Fish Physiology and Biochemistry",
        "title_sort": "Fish Physiology and Biochemistry",
        "eissn": "1573-5168",
        "description": "Fish Physiology and Biochemistry is an international journal publishing original research papers in all aspects of the physiology and biochemistry of fishes. Coverage includes experimental work in such topics as biochemistry of organisms, organs, tissues and cells; structure of organs, tissues, cells and organelles related to their function; nutritional, osmotic, ionic, respiratory and excretory homeostasis; nerve and muscle physiology; endocrinology; reproductive physiology; energetics; biochemical and physiological effects of toxicants; molecular biology and biotechnology and more. The journal presents full papers, brief communications, rapid communications, unsolicited and invited reviews and editorial comments and announcements.",
        "access_model": "hybrid",
        "aims": "Fish Physiology and Biochemistry is an international journal publishing original research papers in all aspects of the physiology and biochemistry of fishes. Papers dealing with experimental work in the following areas will be given preference, but other topics will be given consideration: Biochemistry of organisms, organs, tissues and cells.<br/>Structure of organs, tissues, cells and organelles related to their function.<br/>Nutritional, osmotic, ionic, respiratory and excretory homeostasis.<br/>Nerve and muscle physiology.<br/>Endocrinology. Reproductive physiology.<br/>Energetics.<br/>Biochemical and physiological effects of toxicants.<br/>Molecular biology and biotechnology. <br/>Categories of articles include full papers, brief communications, rapid communications, unsolicited and invited reviews and editorial comments and announcements.",
        "forcode": "0602",
        "generic_image": "0",
        "url": "http://www.springer.com/journal/10695",
        "pissn": "0920-1742",
        "address": "",
        "scie": true,
        "forname": "Ecology",
        "_version_": 1480011178813948000
      },
      {
        "issn": "0026-9255",
        "frequency": 1,
        "impact_factor": 0.698,
        "publisher": "Springer",
        "source": "Springer",
        "id": 5,
        "cover": "http://images.springer.com/sgw/journals/medium/605.jpg",
        "title": "Monatshefte für Mathematik",
        "title_sort": "Monatshefte für Mathematik",
        "eissn": "1436-5081",
        "description": "Founded in 1890,Â Monatshefte fÃ¼r MathematikÂ is devoted to research in mathematics in its broadest sense.The journal publishes research papers of general interest in all areas of mathematics. Surveys of significant developments in the fields of pure and applied mathematics and mathematical physics may be occasionally included. The journal is of interest to all research mathematicians. ",
        "access_model": "hybrid",
        "aims": "Founded in 1890, the journal is devoted to research in mathematics in its broadest sense.The journal publishes research papers of general interest in all areas of mathematics. Surveys of significant developments in the fields of pure and applied mathematics and mathematical physics may be occasionally included. The journal is of interest to all research mathematicians.     ",
        "forcode": "0101",
        "generic_image": "0",
        "url": "http://www.springer.com/journal/605",
        "pissn": "0026-9255",
        "address": "",
        "scie": true,
        "forname": "Pure Mathematics",
        "_version_": 1480011178825482200
      },
      {
        "issn": "1793-6047",
        "frequency": 61,
        "impact_factor": 1.622,
        "publisher": "World Scientific Publishing",
        "source": "World Scientific Publishing",
        "id": 6,
        "cover": "http://www.worldscientific.com/na101/home/literatum/publisher/wspc/journals/covergifs/fml/cover.gif",
        "title": "Functional Materials Letters",
        "title_sort": "Functional Materials Letters",
        "eissn": "1793-7213",
        "description": "Functional Materials Letters is an international peer-reviewed scientific journal for original contributions to research on the synthesis, behavior and characterization of functional materials. The journal seeks to provide a rapid forum for the communication of novel research of high quality and with an interdisciplinary flavor. The journal is an ideal forum for communication amongst materials scientists and engineers, chemists and chemical engineers, and physicists in the dynamic fields associated with functional materials.Functional materials are designed to make use of their natural or engineered functionalities to respond to changes in electrical and magnetic fields, physical and chemical environment, etc. These design considerations are fundamentally different to those relevant for structural materials and are the focus of this journal. Functional materials play an increasingly important role in the development of the field of materials science and engineering.The scope of the journal covers theoretical and experimental studies of functional materials, characterization and new applications-related research on functional materials in macro-, micro- and nano-scale science and engineering. Among the topics covered are ferroelectric, multiferroic, ferromagnetic, magneto-optical, optoelectric, thermoelectric, energy conversion and energy storage, sustainable energy and shape memory materials.",
        "access_model": "hybrid",
        "aims": "",
        "forcode": "0900",
        "generic_image": "1",
        "url": "http://www.worldscientific.com/fml",
        "pissn": "1793-6047",
        "address": "WORLD SCIENTIFIC PUBL CO PTE LTD, 5 TOH TUCK LINK, SINGAPORE, SINGAPORE, 596224",
        "scie": true,
        "forname": "Engineering",
        "_version_": 1480011178832822300
      },
      {
        "issn": "1066-5307",
        "frequency": 91,
        "impact_factor": 0,
        "publisher": "Springer",
        "source": "Springer",
        "id": 7,
        "cover": "http://images.springer.com/sgw/journals/medium/12004.jpg",
        "title": "Mathematical Methods of Statistics",
        "title_sort": "Mathematical Methods of Statistics",
        "eissn": "1934-8045",
        "description": "The journal is dedicated to the mathematical foundations of statistical theory. It primarily publishes research papers with complete proofs and, occasionally, survey papers on particular fields of statistics. Papers dealing with applications of statistics are also published if they contain new theoretical developments to the underlying statistical methods. The journal provides an outlet for research in advanced statistical methodology and for studies where such methodology is effectively used or which stimulate its further development.",
        "aims": "",
        "forcode": "0104",
        "generic_image": "0",
        "url": "http://www.springer.com/journal/12004",
        "pissn": "1066-5307",
        "address": "",
        "scie": false,
        "forname": "Statistics ",
        "_version_": 1480011178840162300
      },
      {
        "issn": "0066-7870",
        "frequency": 0,
        "impact_factor": 0,
        "publisher": "Universidade de Sao Paulo Sistema Integrado de Bibliotecas - SIBiUSP",
        "source": "Universidade de Sao Paulo Sistema Integrado de Bibliotecas - SIBiUSP",
        "id": 8,
        "cover": "",
        "title": "ARQUIVOS DE ZOOLOGIA-SAO PAULO",
        "title_sort": "ARQUIVOS DE ZOOLOGIA-SAO PAULO",
        "eissn": "0066-7870",
        "description": "Formerly “Arquivos de Zoologia do Estado de São Paulo”, v.1 (1940)- v.14 (1966), it is a traditional journal in the Field of Zoology in Brazil. The journal aims to publish original scientific papers in the áreas of systematics, paleontology, evolutionary biology, ecology, taxonomy, anatomy, behavior, functional morphology, ontogeny, studies on fauna and biogeography. Published by the Museu de Zoologia da Universidade de São Paulo (MZUSP), the journal is indexed in the main reference databases and appears every six months.",
        "aims": "",
        "forcode": "",
        "generic_image": "1",
        "url": "http://www.revistasusp.sibi.usp.br/scielo.php?script=sci_serial&pid=0066-7870&lng=en&nrm=iso",
        "pissn": "0066-7870",
        "address": "MUSEU ZOOLOGIA UNIV SAO PAULO, AV NAZARE 481 CAIXA POSTAL 7172, SAO PAULO, BRAZIL, SP, 00000",
        "scie": false,
        "forname": "",
        "_version_": 1480011178846453800
      },
      {
        "issn": "0165-5876",
        "frequency": 30,
        "impact_factor": 1.319,
        "publisher": "Elsevier",
        "source": "Elsevier",
        "id": 9,
        "cover": "http://www.extranet.elsevier.com/inca_covers_store/issn/01655876.gif",
        "title": "International Journal of Pediatric Otorhinolaryngology",
        "title_sort": "International Journal of Pediatric Otorhinolaryngology",
        "eissn": "1872-8464",
        "description": "The purpose of the International Journal of Pediatric Otorhinolaryngology is to concentrate and disseminate information concerning prevention, cure \nand care of otorhinolaryngological disorders in infants and children due to developmental, degenerative, infectious, neoplastic, traumatic, \nsocial, psychiatric and economic causes. The Journal provides a medium for \nclinical and basic contributions in all of the areas of pediatric otorhinolaryngology. This includes medical and surgical otology, bronchoesophagology, \nlaryngology, rhinology, diseases of the head and neck, and disorders of \ncommunication, including voice, speech and language disorders.Published in cooperation with the American Academy of Pediatrics Section on \nOtolaryngology and Bronchoesophagology, the Asociación Argentina de \nOtorrinolaringología y Fonoaudiología Pediátrica, the Association Française \nd'Otorhinolaryngologie Pédiatrique, the Australasian Society of Paediatric \nOto-Rhino-Laryngology, the British Association for Paediatric \nOtorhinolaryngology, the Dutch/FlemishWorking Group for Pediatric \nOtorhinolaryngology, the European Society for Pediatric \nOtorhinolaryngology, the Hungarian Society of Otorhinolaryngologists \nSection on Pediatric Otorhinolaryngology, the Interamerican Association of \nPediatric Otorhinolaryngology, the Italian Society of Pediatric \nOtorhinolaryngology, theJapan Society for Pediatric Otorhinolaryngology, \nthe Polish Society of Pediatric Otorhinolaryngology, and the Society for \nEar, Nose and Throat Advances in Children.",
        "access_model": "hybrid",
        "aims": "The purpose of the International Journal of Pediatric Otorhinolaryngology is to concentrate and disseminate information concerning prevention, cure and care of otorhinolaryngological disorders in infants and children due to developmental, degenerative, infectious, neoplastic, traumatic, social, psychiatric and economic causes. The Journal provides a medium for clinical and basic contributions in all of the areas of pediatric otorhinolaryngology. This includes medical and surgical otology, bronchoesophagology, laryngology, rhinology, diseases of the head and neck, and disorders of communication, including voice, speech and language disorders.Published in cooperation with the American Academy of Pediatrics Section on Otolaryngology and Bronchoesophagology, the Asociación Argentina de Otorrinolaringología y Fonoaudiología Pediátrica, the Association Française d'Otorhinolaryngologie Pédiatrique, the Australasian Society of Paediatric Oto-Rhino-Laryngology, the British Association for PaediatricOtorhinolaryngology, the Dutch/FlemishWorking Group for Pediatric Otorhinolaryngology, the European Society for Pediatric Otorhinolaryngology, the Hungarian Society of Otorhinolaryngologists Section on Pediatric Otorhinolaryngology, the Interamerican Association of Pediatric Otorhinolaryngology, the Italian Society of Pediatric Otorhinolaryngology, theJapan Society for Pediatric Otorhinolaryngology, the Polish Society of Pediatric Otorhinolaryngology, and the Society for Ear, Nose and ThroatAdvances in Children.",
        "forcode": "1114",
        "generic_image": "0",
        "url": "http://www.journals.elsevier.com/international-journal-of-pediatric-otorhinolaryngology",
        "pissn": "0165-5876",
        "address": "ELSEVIER IRELAND LTD, ELSEVIER HOUSE, BROOKVALE PLAZA, EAST PARK SHANNON, CO, CLARE, IRELAND, 00000",
        "scie": true,
        "forname": "Paediatrics And Reproductive Medicine",
        "_version_": 1480011178852745200
      },
      {
        "issn": "1566-5852",
        "frequency": 183,
        "impact_factor": 0.2,
        "publisher": "John Benjamins Publishing Company",
        "source": "John Benjamins Publishing Company",
        "id": 10,
        "cover": "",
        "title": "Journal of Historical Pragmatics",
        "title_sort": "Journal of Historical Pragmatics",
        "eissn": "1569-9854",
        "description": "The Journal of Historical Pragmatics provides an interdisciplinary forum for theoretical, empirical and methodological work at the intersection of pragmatics and historical linguistics. The editorial focus is on socio-historical and pragmatic aspects of historical texts in their sociocultural context of communication (e.g. conversational principles, politeness strategies, or speech acts) and on diachronic pragmatics as seen in linguistic processes such as grammaticalization or discoursization.",
        "aims": "",
        "forcode": "2202",
        "generic_image": "1",
        "url": "http://benjamins.com/#catalog/journals/jhp/main",
        "pissn": "1566-5852",
        "address": "JOHN BENJAMINS PUBLISHING COMPANY, PO BOX 36224, AMSTERDAM, NETHERLANDS, 1020 ME",
        "scie": false,
        "forname": "History And Philosophy Of Specific Fields",
        "_version_": 1480011178871619600
      }
    ]
  }
}
 */
public class JournalInput {
    @Field
    String issn;

    @Field
    Integer id;

    @Field
    String title;

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
