package org.bindiego.solr.data;

import org.apache.solr.client.solrj.beans.Field;

import java.util.List;

/*
schema.xml:

<field name="kw" type="string" indexed="true" stored="true" />
<field name="pinyin" type="string" indexed="true" stored="false" multiValued="true"/>
<field name="abbre" type="string" indexed="true" stored="false" multiValued="true"/>
<field name="kwfreq" type="int" indexed="true" stored="true" />
<field name="_version_" type="long" indexed="true" stored="true"/>
<field name="suggest" type="suggest_text" indexed="true" stored="false" multiValued="true" />
------------------multiValued表示字段是多值的-------------------------------------
<uniqueKey>kw</uniqueKey>
<defaultSearchField>suggest</defaultSearchField>

说明：
kw为原始关键字
pinyin和abbre的multiValued=true,在使用solrj建此索引时，定义成集合类型即可：如关键字“重庆”的pinyin字段为{chongqing,zhongqing}, abbre字段为{cq, zq}
kwfreq为用户搜索关键的频率，用于查询的时候排序

-------------------------------------------------------

<copyField source="kw" dest="suggest" />
<copyField source="pinyin" dest="suggest" />
<copyField source="abbre" dest="suggest" />

------------------suggest_text----------------------------------

<fieldType name="suggest_text" class="solr.TextField" positionIncrementGap="100" autoGeneratePhraseQueries="true">
    <analyzer type="index">
            <tokenizer class="solr.KeywordTokenizerFactory" />
            &lt;filter class="solr.SynonymFilterFactory"
                    synonyms="synonyms.txt"
                    ignoreCase="true"
                    expand="true" />
            <filter class="solr.StopFilterFactory"
                    ignoreCase="true"
                    words="stopwords.txt"
                    enablePositionIncrements="true" />
            <filter class="solr.LowerCaseFilterFactory" />
            <filter class="solr.KeywordMarkerFilterFactory" protected="protwords.txt" />
    </analyzer>
    <analyzer type="query">
            <tokenizer class="solr.KeywordTokenizerFactory" />
            <filter class="solr.StopFilterFactory"
                    ignoreCase="true"
                    words="stopwords.txt"
                    enablePositionIncrements="true" />
            <filter class="solr.LowerCaseFilterFactory" />
            <filter class="solr.KeywordMarkerFilterFactory" protected="protwords.txt" />
    </analyzer>
</fieldType>
*/

public class Journal {
    @Field("id")
    String id;

    @Field("issn")
    String issn;

    @Field("title")
    String title;

    @Field("abbr")
    List<String> abbr;

    /*
    @Field("suggest")
    List<String> suggest;
    */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAbbr() {
        return abbr;
    }

    public void setAbbr(List<String> abbr) {
        this.abbr = abbr;
    }

    /*
    public List<String> getSuggest() {
        return suggest;
    }

    public void setSuggest(List<String> suggest) {
        this.suggest = suggest;
    }
    */
}
