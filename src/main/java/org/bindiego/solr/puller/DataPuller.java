package org.bindiego.solr.puller;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.SolrParams;
import org.bindiego.solr.data.paging.OrderBy;
import org.bindiego.solr.data.paging.Pagination;
import org.bindiego.solr.server.BindiegoSolrServer;

import java.util.List;

/**
 * Created by wubin on 14-9-28.
 */
public class DataPuller {
    static final Logger logger = LogManager.getFormatterLogger(DataPuller.class.getName());

    private SolrServer server;

    public DataPuller(final String url) {
        this.server = BindiegoSolrServer.getSolrServer(url);
    }

    public SolrDocumentList getDocs(SolrQuery query) {
        QueryResponse response = null;

        try {
            response = server.query(query);
        } catch (SolrServerException e) {
            logger.error("", e);
        }

        return response.getResults();
    }

    public <T> Pagination<T> getBeans(SolrQuery query,
            int pageNo, int pageSize, OrderBy orderBy,
            Class<T> clazz) {

        QueryResponse response = null;

        query.setStart(getStart(pageNo, pageSize)).setRows(pageSize);

        if (orderBy != null && orderBy.validate()) {
            for (String str : orderBy.getFields()) {
                query.addSortField(str, orderBy.getOrder());
            }
        }

        response = queryByParams(query);

        List<T> beans = response.getBeans(clazz);

        int totalCount = Long.valueOf(response.getResults().getNumFound()).intValue();

        return new Pagination<T>(pageNo, pageSize, totalCount, beans);
    }

    private int getStart(int pageNo, int pageSize) {
        return (pageNo - 1) * pageSize;
    }

    protected QueryResponse queryByParams(SolrParams params) {
        QueryResponse response = null;

        try {
            response = server.query(params);
        } catch (SolrServerException e) {
            logger.error("", e);
        } finally {
            return response;
        }
    }

    public long getTotalCount() {
        long rtn = 0L;

        SolrQuery query = new SolrQuery(("*:*"));

        query.setRows(0); // don't actually request any data

        try {
            rtn = server.query(query).getResults().getNumFound();
        } catch (SolrServerException e) {
            logger.error("", e);
        } finally {
            return rtn;
        }
    }
}
